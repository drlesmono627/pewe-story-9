from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'loginapp'

urlpatterns = [
    path('', views.login, name='login'),
    path('landing/', views.landing, name='landing'),
    path('register/', views.register, name='register'),
    path('', views.gologout, name="logout"),
]