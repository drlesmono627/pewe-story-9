from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate, login as dj_login
from django.contrib.auth.forms import AuthenticationForm,UserCreationForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def login(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            dj_login(request,user)
            request.session["user"] = request.POST["username"]
            return redirect("/landing/")

    form = AuthenticationForm()
    return render(request, "login.html", {"form":form })

def register(request):
    if request.method == "POST":
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            request.session["user"] = request.POST["username"]
            return redirect("")

    form = UserCreationForm()
    return render(request, "register.html", {"form":form })

@login_required(login_url="loginapp:login")
def landing(request):
    username = request.session.get("user")
    return render(request, "landing.html", {"username":username })

def gologout(request):
    if request.method == "POST":
        logout(request)
    return redirect("login.html")



